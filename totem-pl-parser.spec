Name:		totem-pl-parser
Version:	3.26.6
Release:        1
Summary:	Library fot Totem Playlist Parser
License:	LGPLv2+
URL:		https://wiki.gnome.org/Apps/Videos
Source0:	https://download.gnome.org/sources/%{name}/3.26/%{name}-%{version}.tar.xz

BuildRequires:	meson gtk-doc gettext glib2-devel libxslt
BuildRequires: 	gmime30-devel libxml2-devel libsoup-devel gobject-introspection-devel
BuildRequires:	libquvi-devel libarchive-devel libgcrypt-devel uchardet-devel

%description
Totem-pl-parser is a simple GObject-based library to parse a host of playlist
formats, as well as save those.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for developing
applications that use %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Denable-gtk-doc=true \
	-Denable-libarchive=yes \
	-Denable-libgcrypt=yes \
	-Dintrospection=true
%meson_build

%install
%meson_install
%find_lang %{name} --with-gnome

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%license COPYING.LIB
%doc AUTHORS
%{_libdir}/girepository-1.0/*.typelib
%{_libdir}/*.so.*
%{_libexecdir}/%{name}

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/*.gir

%files help
%doc NEWS
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Wed Apr 20 2022 dillon chen <dillon.chen@gmail.com> - 3.26.6-1
- Update to 3.26.6

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.26.5-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Jan 28 2021 liudabo <liudabo1@huawei.com> - 3.26.5-1
- upgrade version to 3.26.5

* Thu Nov 21 2019 chengquan <chengquan3@huawei.com> - 3.26.1-5
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add buildrequire of libxslt

* Wed Oct 10 2019 luhuaxin <luhuaxin@huawei.com> - 3.26.1-4
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: move AUTHORS to license folder

* Wed Aug 28 2019 luhuaxin <luhuaxin@huawei.com> - 3.26.1-3
- Package init
